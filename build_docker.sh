#!/bin/bash

# grab all the repositories once to avoid redundant pulls for each build
if [[ ! -d ./petsc ]]; then
  echo "Downloading petsc..."
  git clone -b next https://sdalton@bitbucket.org/sdalton/petsc.git
else
  cd petsc && git pull && cd ..
fi

if [[ ! -d ./cusplibrary ]]; then
  echo "Downloading cusplibrary..."
  git clone https://github.com/cusplibrary/cusplibrary.git
else
  cd cusplibrary && git pull && cd ..
fi

if [[ ! -d ./thrust ]]; then
  echo "Downloading thrust..."
  git clone -b 1.7.0 https://github.com/thrust/thrust.git
else
  cd thrust && git pull && cd ..
fi

# if [[ ! -d ./cuda ]]; then
#   CUDA_FILE=cuda_6.0.37_linux_64.run
#
#   if [[ ! -a $CUDA_FILE ]]; then
#     wget http://developer.download.nvidia.com/compute/cuda/6_0/rel/installers/$CUDA_FILE
#   fi
#
#   sh ./$CUDA_FILE --silent -toolkit --override -toolkitpath=`pwd`/cuda
# fi

#simple docker build using Dockerfile
docker build -t fedora-petsc .
