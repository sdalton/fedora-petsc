############################################################
# Dockerfile to build petsc container images
# # Based on Fedora
# ############################################################

# Set the base image to Fedora
FROM fedora:20

# File Author / Maintainer
MAINTAINER Steven Dalton

# Update the repository sources list
# RUN yum -y update

################## BEGIN INSTALLATION ######################
# Install the required software
RUN yum -y install blas environment-modules git gcc gcc-c++ kernel-devel lapack-devel make openmpi-devel perl perl-Env scons unzip valgrind-devel vim wget
RUN yum clean all

# Grab the repositories
ADD ./cusplibrary /cusplibrary
ADD ./petsc /petsc
ADD ./thrust /thrust

# Set extra configuration files
ADD ./bashrc /root/.bashrc
ADD ./petsc_configure.sh /petsc/petsc_configure.sh
ADD ./petsc_test.sh /petsc/petsc_test.sh

##################### INSTALLATION END #####################
RUN printf "source /root/.bashrc\n" >> /etc/profile
