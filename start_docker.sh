#!/bin/bash

CUDA_DIR=/usr/local/cuda
#CUDA_DIR=/usr/lib/nvidia-cuda-toolkit

# Check for nvcc
if [[ ! -f $CUDA_DIR/bin/nvcc ]]; then
  echo "Could not find nvcc!"
  #exit 1
fi

# Locate required libcuda.so.1 file using ldconfig
LIB_FILE=`ldconfig -p | grep ".*64.*libcuda\.so\.1" | grep -oh "\/.*\/libcuda\.so\.1"`
if [[ -z $LIB_FILE ]]; then
  echo "Could not find libcuda!"
  exit 1
fi

# Make sure nvidia_uvm module is loaded before attempting to run any kernel in docker
UVM_CHECK=`lsmod | grep ^nvidia_uvm`
if [[ -z $UVM_CHECK ]]; then
  echo "UVM module required for docker CUDA support is not loaded!"
  echo "Attempting to load now..."
  sudo nvidia-modprobe -u
fi

# start docker instance with CUDA support
CMD="docker run --privileged -v $LIB_FILE:/usr/lib64/libcuda.so.1 -v $CUDA_DIR:/usr/local/cuda -i -t fedora-petsc /bin/bash -l"
echo "Running : $CMD"
$CMD
