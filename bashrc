# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias ls='ls --color'

# Source global definitions
if [ -f /etc/bashrc ]; then
. /etc/bashrc
fi

# Add cuda directories
export PATH=$PATH:/usr/local/cuda/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64

# Required for linker to find /usr/lib64/libcuda.so.1
sudo ldconfig
# Required for MPI
module load mpi/openmpi-x86_64
