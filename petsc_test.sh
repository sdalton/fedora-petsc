#!/bin/bash

DIR=src/ksp/ksp/examples/tutorials
CMD="./ex2 -m 122 -n 605 -ksp_type cg -ksp_rtol 1.0e-8 -ksp_max_it 100000 -log_summary -pc_type none -vec_type cusp -mat_type aijcusp -mat_cusp_storage_format ell"

pushd src/ksp/ksp/examples/tutorials
make PETSC_DIR=/petsc PETSC_ARCH=arch-linux2-c-opt ex2
$CMD
popd

