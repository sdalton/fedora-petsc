#!/bin/bash

/petsc/configure --with-thrust-dir=/thrust --with-cusp-dir=/cusplibrary --with-cuda-dir=/usr/local/cuda --with-cuda-arch=sm_35 --download-cmake=yes --download-fblaslapack=yes --with-shared-libraries --with-debugging=0 --with-make-np=8 --FOPTFLAGS=-O3 --COPTFLAGS=-O3 --CXXOPTFLAGS=-O3 --CUDAOPTFLAGS=-O3 --with-c2html=0 --with-large-file-io=1 && make -C /petsc PETSC_DIR=/petsc PETSC_ARCH=arch-linux2-c-opt all
