#!/bin/bash

# Stop all running containers
docker stop $(docker ps -a -q)

# Remove all containers
docker rm $(docker ps -a -q)

# Remove all unnamed images
docker images | grep "<none>" | awk '{print $3}' | xargs docker rmi
